﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotator : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

		Vector3 rotator = new Vector3(15, 30, 45) * Time.deltaTime;

		transform.Rotate(rotator);

	}
}
