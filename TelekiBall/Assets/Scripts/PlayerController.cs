﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class PlayerController : MonoBehaviour {

	public GameObject bulletPrefab;
	public MeshRenderer bodyMeshRenderer;
	public Transform bulletSpawn;
	public Camera myCamera;

	private void Start() {
		if (!isLocalPlayer) {
			myCamera.enabled = false;
		}
	}

	public override void OnStartLocalPlayer() {

		bodyMeshRenderer.material.color = Color.blue;

	}

	private void Update() {

		if (!isLocalPlayer) {
			return;
		}

		float x = Input.GetAxis("Horizontal") * Time.deltaTime * 150.0f;
		float z = Input.GetAxis("Vertical") * Time.deltaTime * 3.0f;

		transform.Rotate(0, x, 0);
		transform.Translate(0, 0, z);
		//transform.Translate(0, 0, z);

		if (Input.GetKeyDown(KeyCode.Space)) {
			CmdFire();
		}

	}

	[Command]
	private void CmdFire() {

		GameObject bullet = Instantiate(bulletPrefab, bulletSpawn.position, bulletSpawn.rotation);

		bullet.GetComponent<Rigidbody>().velocity = bullet.transform.forward * 6;

		NetworkServer.Spawn(bullet);

		Destroy(bullet, 2.0f);

	}

}
