﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class Health : MonoBehaviour {

	public const int maxHealth = 100;
	public bool destroyOnDeath;

	[SyncVar(hook = "OnChangeHealth")]
	public int currentHealth = maxHealth;
	public RectTransform healthBar;

	private NetworkStartPosition[] spawnPoints;

	private void Start() {
		
		if (isLocalPlayer) {
			spawnPoints = FindObjectsOfType<NetworkStartPosition>();
		}

	}

	public void TakeDamage(int amount) {
		if (!isServer) {
			return;
		}

		currentHealth -= amount;

		if (currentHealth <= 0) {

			if (destroyOnDeath) {

				Destroy(gameObject);

			} else {

				currentHealth = maxHealth;

				RpcRespawn();
			}

		}

	}

	private void OnChangeHealth(int currentHealth) {
		healthBar.sizeDelta = new Vector2(currentHealth, healthBar.sizeDelta.y);
	}

	[ClientRpc]
	private void RpcRespawn() {

		if (isLocalPlayer) {
			Vector3 spawnPoint = Vector3.zero;

			if (spawnPoints != null && spawnPoints.Length > 0) {
				spawnPoint = spawnPoints[Random.Range(0, spawnPoints.Length)].transform.position;
			}

			transform.position = spawnPoint;

		}

	}


	
}
