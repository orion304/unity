﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

	private static int playerNumber = 1;

	public float speed = 5.0f;
	public float rotateSpeed = 180.0f;

	private string playerPrefix = "";
	private string horizontalAxis = "";
	private string verticalAxis = "";
	private string lookXAxis = "";
	private string lookYAxis = "";

	private void Awake() {

		playerPrefix = "P" + playerNumber + "_";
		playerNumber++;

		SetUpAxes();
	}

	private void SetUpAxes() {
		horizontalAxis = playerPrefix + "Horizontal";
		verticalAxis = playerPrefix + "Vertical";
		lookXAxis = playerPrefix + "Look_X";
		lookYAxis = playerPrefix + "Look_Y";
	}

	private void Update() {
		Move();
		Look();
	}

	private void Move() {

		float x = Input.GetAxis(horizontalAxis);
		float y = Input.GetAxis(verticalAxis);

		Vector3 movement = new Vector3(x, 0, y) * speed * Time.deltaTime;

		transform.Translate(movement);

		//Debug.Log("Test " + movement.magnitude);

	}

	private void Look() {

		float x = Input.GetAxis(lookXAxis);
		float y = Input.GetAxis(lookYAxis);

		transform.Rotate(0, x * rotateSpeed * Time.deltaTime, 0);

	}

	
}
