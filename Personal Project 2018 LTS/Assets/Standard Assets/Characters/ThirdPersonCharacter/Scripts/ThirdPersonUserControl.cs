using System;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

namespace UnityStandardAssets.Characters.ThirdPerson
{
    [RequireComponent(typeof (ThirdPersonCharacter))]
    public class ThirdPersonUserControl : MonoBehaviour
    {

        public RectTransform Reticle;

        public GameObject EarthWallPrefab;
        public GameObject AirBlastPrefab;

        private ThirdPersonCharacter m_Character; // A reference to the ThirdPersonCharacter on the object
        private Transform m_Cam;                  // A reference to the main camera in the scenes transform
        private Vector3 m_CamForward;             // The current forward direction of the camera
        private Vector3 m_Move;
        private bool m_Jump;                      // the world-relative desired move direction, calculated from the camForward and user input.

        private bool _fire1;
        private bool _fire2;
        private bool _fire3;
        
        private void Start()
        {
            // get the transform of the main camera
            if (Camera.main != null)
            {
                m_Cam = Camera.main.transform;
            }
            else
            {
                Debug.LogWarning(
                    "Warning: no main camera found. Third person character needs a Camera tagged \"MainCamera\", for camera-relative controls.", gameObject);
                // we use self-relative controls in this case, which probably isn't what the user wants, but hey, we warned them!
            }

            // get the third person character ( this should never be null due to require component )
            m_Character = GetComponent<ThirdPersonCharacter>();
        }


        private void Update()
        {
            if (!m_Jump)
            {
                m_Jump = CrossPlatformInputManager.GetButtonDown("Jump");
            }

            if (!_fire1)
            {
                _fire1 = CrossPlatformInputManager.GetButtonDown("Fire1");
            }

            if (!_fire2)
            {
                _fire2 = CrossPlatformInputManager.GetButtonDown("Fire2");
            }

            if (!_fire3)
            {
                _fire3 = CrossPlatformInputManager.GetButtonDown("Fire3");
            }
        }


        // Fixed update is called in sync with physics
        private void FixedUpdate()
        {

            if (_fire1 || _fire2)
            {
                UseAbility();
            }

            // read inputs
            float h = CrossPlatformInputManager.GetAxis("Horizontal");
            float v = CrossPlatformInputManager.GetAxis("Vertical");
            var x = CrossPlatformInputManager.GetAxis("Mouse X");
            bool crouch = Input.GetKey(KeyCode.C);
            var forward = Vector3.forward;

            // calculate move direction to pass to character
            if (m_Cam != null)
            {
                // calculate camera relative direction to move:
                forward = m_Cam.forward;
                m_CamForward = Vector3.Scale(m_Cam.forward, new Vector3(1, 0, 1)).normalized;
                m_Move = v*m_CamForward + h*m_Cam.right;
            }
            else
            {
                // we use world-relative directions in the case of no main camera
                m_Move = v*Vector3.forward + h*Vector3.right;
            }
#if !MOBILE_INPUT
			// walk speed multiplier
	        if (Input.GetKey(KeyCode.LeftShift)) m_Move *= 0.5f;
#endif

            // pass all parameters to the character control script
            m_Character.Move(m_Move, x, crouch, m_Jump);
            m_Jump = false;
        }

        private void UseAbility()
        {
            if (_fire1)
            {
                _fire1 = false;
                AirBlast();
            }

            if (_fire2)
            {
                _fire2 = false;
                EarthWall();
            }

            if (_fire3)
            {
                ToggleCursorLock();
            }
        }

        private void EarthWall()
        {
            if (m_Cam == null) return;
            var source = m_Cam.transform.position;
            Physics.Raycast(source, m_Cam.forward * 50, out var hit);
            if (hit.collider == null) return;
            if (hit.collider.GetComponent<Rigidbody>() != null) return;

            var forward = Vector3.Cross(hit.normal, Vector3.Cross(m_Cam.forward, hit.normal));

            Instantiate(EarthWallPrefab, hit.point, Quaternion.LookRotation(forward, hit.normal));
        }

        private void AirBlast()
        {
            if (m_Cam == null) return;
            var source = m_Cam.transform.position;
            var distanceToPlayer = (gameObject.transform.position - source).magnitude;
            source += m_Cam.forward * distanceToPlayer;
            Instantiate(AirBlastPrefab, source, m_Cam.rotation);
        }

        private void ToggleCursorLock()
        {
            SetCursorLock(Cursor.visible);
        }

        private void SetCursorLock(bool cursorLocked)
        {
            Cursor.visible = !cursorLocked;
            Cursor.lockState = cursorLocked ? CursorLockMode.Locked : CursorLockMode.None;
        }
    }
}
