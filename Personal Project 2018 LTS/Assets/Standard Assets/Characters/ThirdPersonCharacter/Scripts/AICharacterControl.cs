using System;
using System.Collections.Generic;
using UnityEngine;

namespace UnityStandardAssets.Characters.ThirdPerson
{
    [RequireComponent(typeof (UnityEngine.AI.NavMeshAgent))]
    [RequireComponent(typeof (ThirdPersonCharacter))]
    public class AICharacterControl : MonoBehaviour
    {
        private static readonly System.Random Random = new System.Random();

        public UnityEngine.AI.NavMeshAgent agent { get; private set; }             // the navmesh agent required for the path finding
        public ThirdPersonCharacter character { get; private set; } // the character we are controlling
        public Transform target;                                    // target to aim for

        private GameObject[] _aiGoals;

        private void Start()
        {
            // get the components on the object we need ( should not be null due to require component so no need to check )
            agent = GetComponentInChildren<UnityEngine.AI.NavMeshAgent>();
            character = GetComponent<ThirdPersonCharacter>();

	        agent.updateRotation = false;
	        agent.updatePosition = true;

            _aiGoals = GameObject.FindGameObjectsWithTag("AI Goal");
        }

        private void SetRandomTarget()
        {
            if (_aiGoals == null) return;

            target = _aiGoals[Random.Next(_aiGoals.Length)].transform;
        }

        private void Update()
        {
            var forward = 0f;

            if (target == null)
                SetRandomTarget();

            if (target != null)
            {
                agent.SetDestination(target.position);
                var angle = Vector3.SignedAngle(transform.forward, agent.desiredVelocity, Vector3.up) / 10;
                forward = Mathf.Clamp(angle, -1, 1);
            }
            

            if (agent.remainingDistance > agent.stoppingDistance)
                character.Move(agent.desiredVelocity, forward, false, false);
            else
            {
                character.Move(Vector3.zero, forward, false, false);
                SetRandomTarget();
            }
        }


        public void SetTarget(Transform target)
        {
            this.target = target;
        }
    }
}
