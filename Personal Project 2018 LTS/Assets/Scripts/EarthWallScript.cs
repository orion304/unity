﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EarthWallScript : MonoBehaviour
{

    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void Awake()
    {
        Debug.Log($"Destroying {gameObject} after 2 seconds.");
        Destroy(gameObject, 2f);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
