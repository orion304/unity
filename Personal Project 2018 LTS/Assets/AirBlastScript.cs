﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AirBlastScript : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void Awake()
    {
        Debug.Log($"Destroying {gameObject} after 2 seconds.");
        Destroy(gameObject, 2f);
    }

    private void OnTriggerStay(Collider other)
    {
        var v = other.transform.position - gameObject.transform.position;
        v.Normalize();

        var launchVector = gameObject.transform.forward + 0.2 * v;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
