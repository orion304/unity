﻿using Assets.Scripts;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class AirBlast : NetworkBehaviour {

	public GameObject owner;

	public float pushForce = 10000f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	private void OnTriggerStay(Collider other) {
		if (!isServer) {
			return;
		}

		if (Equals(other.gameObject.transform.root.gameObject, owner)) return;

		var colliderRigidBody = other.gameObject.GetComponentInTree<Rigidbody>();

		if (colliderRigidBody == null) return;

		colliderRigidBody.AddForce(transform.forward.normalized * pushForce, ForceMode.Force);
	}
}
