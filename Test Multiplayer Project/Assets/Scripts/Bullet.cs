﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

	private void OnCollisionEnter(Collision collision) {
		Destroy(gameObject);

		NetworkHealth health = collision.gameObject.GetComponentInParent<NetworkHealth>();
		if (health != null ) {
			health.TakeDamage(10);
		}
	}

}
