﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class PlayerController : NetworkBehaviour {

	public float airBlastVelocity = 10f;
	public float airBlastPushForce = 5000f;
	public float airBlastLifetime = 10f;

	public GameObject bulletPrefab;
	public Transform bulletSpawn;

	public GameObject airBlastPrefab;

	

	private void Update() {
		if (!isLocalPlayer) {
			return;
		}


		if (Input.GetButtonDown("Fire1")) {
			CmdCastAirBlast();
		}
	}

	[Command]
	private void CmdCastAirBlast() {
		var airBlastPrefab = Instantiate(this.airBlastPrefab, bulletSpawn.position, bulletSpawn.rotation);
		var airBlast = airBlastPrefab.GetComponentInChildren<AirBlast>();

		airBlast.pushForce = airBlastPushForce;
		airBlast.owner = gameObject;

		airBlastPrefab.GetComponent<Rigidbody>().velocity = airBlastPrefab.transform.forward.normalized * airBlastVelocity;

		NetworkServer.Spawn(airBlastPrefab);

		Destroy(airBlastPrefab, airBlastLifetime);
	}

	[Command]
	private void CmdFire() {
		var bullet = Instantiate(bulletPrefab, bulletSpawn.position, bulletSpawn.rotation);

		bullet.GetComponent<Rigidbody>().velocity = bullet.transform.forward * 6;

		NetworkServer.Spawn(bullet);

		Destroy(bullet, 2.0f);
	}

}
