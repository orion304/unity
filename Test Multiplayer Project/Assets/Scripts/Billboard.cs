﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Networking;

public class Billboard : MonoBehaviour {

	private void Update() {
		var enabledCamera = Camera.allCameras.FirstOrDefault(c => c.enabled);
		if (enabledCamera == null) {
			//transform.LookAt(Camera.main.transform);
		} else {
			transform.LookAt(enabledCamera.transform);
		}
	}

}
