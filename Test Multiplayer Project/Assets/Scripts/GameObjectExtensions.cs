﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts {
	public static class GameObjectExtensions {

		public static T GetComponentInTree<T>(this GameObject gameObject) {
			return gameObject.transform.root.gameObject.GetComponentInChildren<T>();
		}

	}
}
