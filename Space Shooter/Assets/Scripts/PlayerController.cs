﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Boundary {
	public float xMin, xMax, zMin, zMax;
}

public class PlayerController : MonoBehaviour {

	public float speed = 1.0f;
	public float tilt = 30;
	public float fireRate = 0.5f;

	public GameObject shot;
	public Transform shotSpawnTransform;

	public Boundary boundary;

	private Rigidbody rb;
	private AudioSource audio;
	private float nextFire = 0.0f;

	private void Start() {

		rb = GetComponent<Rigidbody>();
		audio = GetComponent<AudioSource>();

	}

	private void Update() {

		if (Input.GetButton("Fire1") && Time.time > nextFire) {
			nextFire = Time.time + fireRate;
			Instantiate(shot, rb.transform);
			audio.Play();
		}

	}

	private void FixedUpdate() {

		float moveHorizontal = Input.GetAxis("Horizontal");
		float moveVertical = Input.GetAxis("Vertical");

		Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);

		rb.velocity = movement * speed;
		rb.rotation = Quaternion.Euler(0.0f, 0.0f, rb.velocity.x * -tilt);

		float x, y, z;
		x = Mathf.Clamp(rb.position.x, boundary.xMin, boundary.xMax);
		y = 0;
		z = Mathf.Clamp(rb.position.z, boundary.zMin, boundary.zMax);

		rb.position = new Vector3(x, y, z);

	}

}
