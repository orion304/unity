﻿using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class TelekineticableCreature : Telekineticable {

	private NavMeshAgent agent;
	private float originalDrag;
	private bool originalState;
	private bool isAffected = false;
	private bool waitingToRegainControl = false;

	public override void BeginTelekinesis(TelekinesisController controller, Vector3 centerOfEffect) {
		isAffected = true;
		originalDrag = rigidBody.drag;
		rigidBody.drag = 0;

		agent = GetComponent<NavMeshAgent>();
		if (!waitingToRegainControl) {
			originalState = agent.enabled;
		}
		agent.enabled = false;

		base.BeginTelekinesis(controller, centerOfEffect);
	}

	public override void StopTelekinesis() {
		base.StopTelekinesis();
		rigidBody.drag = originalDrag;
		isAffected = false;
		waitingToRegainControl = true;
	}

	private void OnCollisionStay(Collision collision) {
		if (!isAffected && waitingToRegainControl && collision.gameObject.CompareTag("Floor") && (damageable == null || !damageable.isDead)) {
				agent.enabled = originalState;
		}
	}
	

}
