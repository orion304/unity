﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TelekinesisController : MonoBehaviour {

	private static readonly Vector3 viewportVector = new Vector3(0.5f, 0.5f, 0);
	internal static readonly float minVelocity = 3f;
	internal static readonly float minForceMagnitude = .1f;
	internal static readonly float unaffectedDistance = .1f;
	
	private static readonly float scrollSensitivity = 3f;
	private static readonly float shiftMoveSensitivity = 1f;
	private static readonly float minRadiusOfEffect = .05f;
	private static readonly float minDistanceToCenterOfEffect = 2f;
	internal static readonly LayerMask affectedObjectLayer = 2;

	public float maxPower = 20000f;
	public float powerRegenPerSecond = 20000000f;
	public float telekinesisRange = 100f;
	public float maxForce = 500f;
	public float throwForce = 200f;
	public float throwForcePowerMultiplier = 2f;
	public ParticleSystem selectedGlowParticles;
	public ParticleSystem targetedGlowParticles;
	public Slider powerSlider;
	public GameObject sphereOfInfluence;

	private Camera playerCamera;

	internal float power;
	private bool isTelekinesisFree;
	private float freeTelekinesisTimeEnd;
	private Vector3 centerOfEffect;
	private float currentDistanceToCenterOfEffect;
	private float currentRadiusOfEffect = 1f;
	private List<Telekineticable> affectedObjects = new List<Telekineticable>();

	private List<Telekineticable> targetedObjects = new List<Telekineticable>();
	private GameObject currentSphereOfInfluence = null;

	private bool hasAltFired = false;
	private bool sphereOfInfluenceForcedVisible = false;

	private void Start() {

		playerCamera = GetComponentInChildren<Camera>();
		
		power = maxPower;

	}

	private void Update() {

		if (isTelekinesisFree) {
			if (Time.time > freeTelekinesisTimeEnd) {
				isTelekinesisFree = false;
			} else {
				//TODO HUD
			}
		}

		if (Input.GetButtonDown("AltFire")) {
			hasAltFired = true;
			return;
		}

		if (Input.GetButtonDown("ToggleSphereOfInfluence")) {
			sphereOfInfluenceForcedVisible = !sphereOfInfluenceForcedVisible;
		}

		if (affectedObjects.Count == 0) {
			DisplayRadiusOfEffect();
		}

		if (Input.GetButtonDown("ToggleTelekinesis")) {

			if (affectedObjects.Count > 0) {
				StopTelekinesis();
			} else {
				BeginTelekinesis();
			}
		}

		HandlePushPullExpandContract();
		HandlePullTogetherPushApart();

		if (power <= 0) {
			StopTelekinesis();
			power = 0;
		}

		if (power < maxPower) {
			float factor = 1f;
			if (affectedObjects.Count > 0) {
				factor = .1f;
			}
			power += powerRegenPerSecond * factor * Time.deltaTime;
			if (power >= maxPower) {
				power = maxPower;
			}
		} 

		UpdateHUD();
	}

	private void HandleSphereOfInfluence(Vector3 target) {

		bool shouldShow = affectedObjects.Count == 0 && (sphereOfInfluenceForcedVisible || Input.GetButton("PushPullExpandContract"));

		if (shouldShow) {

			if (currentSphereOfInfluence == null) {
				currentSphereOfInfluence = Instantiate(sphereOfInfluence);
			}

			currentSphereOfInfluence.transform.position = target;
			float scale = currentRadiusOfEffect * 2;
			currentSphereOfInfluence.transform.localScale = new Vector3(scale, scale, scale);

		} else {

			if (currentSphereOfInfluence != null) {
				Destroy(currentSphereOfInfluence);
			}

		}

	}

	private void UpdateHUD() {

		powerSlider.normalizedValue = power / maxPower;

	}

	private void HandlePushPullExpandContract() {
		float scroll = Input.GetAxis("Mouse ScrollWheel") * scrollSensitivity;

		if (Input.GetButton("PushPullExpandContract")) {
			scroll += Input.GetAxis("Mouse Y") * shiftMoveSensitivity;
		}

		if (scroll != 0) {
			if (affectedObjects.Count > 0) {

				if (currentDistanceToCenterOfEffect + scroll <= minDistanceToCenterOfEffect) {
					currentDistanceToCenterOfEffect = minDistanceToCenterOfEffect;
				} else {
					currentDistanceToCenterOfEffect += scroll;
				}

				if (currentDistanceToCenterOfEffect > telekinesisRange) {
					currentDistanceToCenterOfEffect = telekinesisRange;
				}
			} else {
				if (currentRadiusOfEffect + scroll <= minRadiusOfEffect) {
					currentRadiusOfEffect = minRadiusOfEffect;
				} else {
					currentRadiusOfEffect += scroll;
				}

				if (currentRadiusOfEffect > telekinesisRange) {
					currentRadiusOfEffect = telekinesisRange;
				}

			}
		}

		
	}

	private void DisplayRadiusOfEffect() {
		Vector3 origin;
		RaycastHit hit;

		List<int> indicesToKeep = new List<int>();
		Telekineticable targetedObject;
		int i;
		int initialTargetedObjectsCount = targetedObjects.Count;

		if (GetTarget(telekinesisRange, out origin, out hit)) {

			HandleSphereOfInfluence(hit.point);

			foreach (Collider collider in Physics.OverlapSphere(hit.point, currentRadiusOfEffect)) {
				if (collider.isTrigger) {
					continue;
				}
				targetedObject = collider.GetComponent<Telekineticable>();
				if (targetedObject != null) {
					i = targetedObjects.IndexOf(targetedObject);
					if (i == -1) {
						targetedObject.Target(this);
						targetedObjects.Add(targetedObject);
					} else {
						indicesToKeep.Add(i);
					}
				}
			}

		}
		
		indicesToKeep.Sort();
		int lastIndex = indicesToKeep.Count - 1;
		for (i = initialTargetedObjectsCount - 1; i >= 0; i--) {
			
			if (lastIndex >= 0 && i == indicesToKeep[lastIndex]) {
				lastIndex--;
			} else {
				targetedObjects[i].ClearTarget();
				targetedObjects.RemoveAt(i);
			}
		}

	}

	private void CancelTargetingParticles() {
		foreach (Telekineticable targetedObject in targetedObjects) {
			targetedObject.ClearTarget();
		}
		targetedObjects.Clear();

	}

	private void HandlePullTogetherPushApart() {
		if (Input.GetButton("PullTogetherPushApart") && affectedObjects.Count > 0) {
			float scroll = -Input.GetAxis("Mouse Y") * .3f;
			if (scroll < -.9f) {
				scroll = -.9f;
			}
			
			foreach (Telekineticable affectedObject in affectedObjects) {
				affectedObject.AlterOffset(scroll);
			}
		}
	}

	private void FixedUpdate() {
		HandleObjects();

		if (hasAltFired) {
			hasAltFired = false;
			AltFire();
		}
	}

	private bool GetTarget(float range, out Vector3 rayOrigin, out RaycastHit hit) {

		rayOrigin = GetCameraCenter();
		return Physics.Raycast(rayOrigin, playerCamera.transform.forward, out hit, range);
		
		
	}

	private void AltFire() {
		
		Vector3 origin;
		RaycastHit hit;

		Vector3 targetPoint;
		if (GetTarget(telekinesisRange, out origin, out hit)) {
			targetPoint = hit.point;
		} else {
			targetPoint = origin + playerCamera.transform.forward * telekinesisRange;
		}

		float throwForceFactor = 1f;
		float powerRequired = affectedObjects.Count * throwForce * throwForcePowerMultiplier;

		if (powerRequired > power) {
			throwForceFactor = power / powerRequired;
			powerRequired = power;
		}

		float force = throwForce * throwForceFactor;

		foreach (Telekineticable affectedObject in affectedObjects) {
			affectedObject.Throw(targetPoint, force);
		}

		if (!isTelekinesisFree) {
			power -= powerRequired;
		}

		StopTelekinesis();
	}

	private Vector3 GetCameraCenter() {
		return playerCamera.ViewportToWorldPoint(viewportVector);
	}

	private void HandleObjects() {

		if (affectedObjects.Count == 0) {
			return;
		}


		if (!Input.GetButton("Look")) {
			centerOfEffect = GetCameraCenter() + playerCamera.transform.forward * currentDistanceToCenterOfEffect;
		}

		float totalNeededPower = 0f;

		foreach (Telekineticable affectedObject in affectedObjects) {
			totalNeededPower += affectedObject.HandleTelekinesis(centerOfEffect);
		}

		if (!isTelekinesisFree) {
			power -= totalNeededPower;
		}
		
	}

	private void BeginTelekinesis() {

		if (currentSphereOfInfluence != null) {
			Destroy(currentSphereOfInfluence);
		}
		StopTelekinesis();
		CancelTargetingParticles();

		Vector3 rayOrigin;
		RaycastHit hit;

		Telekineticable targetedObject;

		if (GetTarget(telekinesisRange, out rayOrigin, out hit)) {

			currentDistanceToCenterOfEffect = (hit.point - rayOrigin).magnitude;

			foreach (Collider collider in Physics.OverlapSphere(hit.point, currentRadiusOfEffect)) {
				if (collider.isTrigger) {
					continue;
				}

				targetedObject = collider.GetComponent<Telekineticable>();
				if (targetedObject != null) {
					targetedObject.BeginTelekinesis(this, hit.point);
					affectedObjects.Add(targetedObject);
				}
			}

		}

	}

	private void StopTelekinesis() {
		
		foreach (Telekineticable affectedObject in affectedObjects) {
			affectedObject.StopTelekinesis();
		}

		affectedObjects.Clear();

	}

	internal void StartFreeTelekinesis(float duration) {
		if (isTelekinesisFree) {
			freeTelekinesisTimeEnd += duration;
		} else {
			freeTelekinesisTimeEnd = Time.time + duration;
		}
		
		isTelekinesisFree = true;
	}

}
