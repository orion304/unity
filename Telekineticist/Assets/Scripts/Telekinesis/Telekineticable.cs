﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(Collider))]
public abstract class Telekineticable : MonoBehaviour {

	private TelekinesisController controller;
	private Vector3 offset;
	private int originalLayer;
	protected Rigidbody rigidBody;
	private ParticleSystem affectedSystem;
	private ParticleSystem targetedSystem;
	private MeshRenderer meshRenderer;
	protected Damageable damageable;
	private Vector3 gravityForce;

	private void Start() {
		rigidBody = GetComponent<Rigidbody>();
		meshRenderer = GetComponent<MeshRenderer>();
		damageable = GetComponent<Damageable>();

		gravityForce = Physics.gravity * rigidBody.mass;

		if (meshRenderer == null) {
			meshRenderer = GetComponentInChildren<MeshRenderer>();
		}
	}

	public virtual void Target(TelekinesisController controller) {
		if (targetedSystem != null) {
			return;
		}
		
		targetedSystem = Instantiate(controller.targetedGlowParticles, gameObject.transform);
		ParticleSystem.ShapeModule shapeModule = targetedSystem.shape;
		shapeModule.meshRenderer = meshRenderer;
	}

	public virtual void ClearTarget() {
		targetedSystem.transform.parent = null;
		Destroy(targetedSystem.gameObject);

		targetedSystem = null;
	}

	/// <summary>
	/// 
	/// </summary>
	/// <param name="centerOfEffect"></param>
	public virtual void BeginTelekinesis(TelekinesisController controller, Vector3 centerOfEffect) {

		if (affectedSystem != null) {
			return;
		}

		this.controller = controller;
		offset = gameObject.transform.position - centerOfEffect;
		originalLayer = gameObject.layer;

		gameObject.layer = TelekinesisController.affectedObjectLayer;

		affectedSystem = Instantiate(controller.selectedGlowParticles, gameObject.transform);
		ParticleSystem.ShapeModule shapeModule = affectedSystem.shape;
		shapeModule.meshRenderer = meshRenderer;

	}

	/// <summary>
	/// Handles the object's movement via telekinesis and returns the amount of force required to move it for power consumption.
	/// </summary>
	/// <param name="centerOfEffect">The center of the telekinetic effect</param>
	/// <returns>The amount of power required to move it</returns>
	public virtual float HandleTelekinesis(Vector3 centerOfEffect) {

		Vector3 desiredVelocity;
		Vector3 desiredPosition;
		Vector3 neededForce;
		float neededForceMagnitude;
		Vector3 distance;
		float d, d2;

		desiredPosition = centerOfEffect + offset;

		distance = desiredPosition - gameObject.transform.position;
		d = distance.magnitude;

		if (d <= TelekinesisController.unaffectedDistance) {
			desiredVelocity = Vector3.zero;
		} else {
			d2 = d - TelekinesisController.unaffectedDistance;
			desiredVelocity = distance.normalized * (d2 + TelekinesisController.minVelocity);
		}

		neededForce = rigidBody.mass * (desiredVelocity - rigidBody.velocity) / Time.deltaTime;

		neededForceMagnitude = neededForce.magnitude;
		if (neededForceMagnitude > controller.maxForce) {
			neededForce *= controller.maxForce / neededForceMagnitude;
			neededForceMagnitude = controller.maxForce;
		}


		if (neededForceMagnitude <= TelekinesisController.minForceMagnitude) {
			neededForce = Vector3.zero;
			neededForceMagnitude = 0;
		}

		neededForce -= gravityForce;

		rigidBody.AddForce(neededForce);


		return neededForce.magnitude * Time.deltaTime;

	}

	public virtual void Throw(Vector3 targetPoint, float throwForce) {
		Vector3 force = targetPoint - gameObject.transform.position;
		force = force.normalized * throwForce;
		rigidBody.AddForce(force, ForceMode.Impulse);
	}

	public virtual void AlterOffset(float scroll) {
		if (scroll > 0 || offset.sqrMagnitude > 1) {
			offset *= (1 + scroll);
		}
	}

	public virtual void StopTelekinesis() {

		gameObject.layer = originalLayer;
		affectedSystem.transform.parent = null;
		Destroy(affectedSystem.gameObject);
		affectedSystem = null;
	}

	private void DamageViaCollision(Collision collision) {

		float damage = 0f;

		if (damageable != null) {
			damage = collision.impulse.magnitude;
			damageable.Damage(damage);
		}

		Rigidbody otherRigidbody = collision.rigidbody;
		Damageable otherDamageable = collision.gameObject.GetComponent<Damageable>();

		if (otherDamageable != null) {
			if (damageable == null) {
				damage = collision.impulse.magnitude;
			}
			otherDamageable.Damage(damage);
		}
		
	}

	private void OnCollisionEnter(Collision collision) {
		DamageViaCollision(collision);
	}

	private void OnCollisionStay(Collision collision) {
		DamageViaCollision(collision);
	}

}
