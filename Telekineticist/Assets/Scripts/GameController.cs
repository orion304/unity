﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour {

	private static float minPowerupSpawnInterval = 10f;
	private static float maxPowerupSpawnInterval = 30f;
	private static float minMonsterSpawnInterval = 15f;
	private static float maxMonsterSpawnInterval = 60f;

	private float nextPowerupSpawnTime;
	private float nextMonsterSpawnTime;

	private void Awake() {
		GetNextMonsterSpawnTime();
		GetNextPowerupSpawnTime();
	}

	private void Update() {
		
		if (Time.time > nextPowerupSpawnTime) {
			SpawnPowerup();
			GetNextPowerupSpawnTime();
		}

		if (Time.time > nextMonsterSpawnTime) {
			SpawnMonster();
			GetNextMonsterSpawnTime();
		}

	}

	private void SpawnMonster() {
		//TODO
	}

	private void SpawnPowerup() {
		//TODO
	}

	private void GetNextPowerupSpawnTime() {
		nextPowerupSpawnTime = Time.time + Random.Range(minPowerupSpawnInterval, maxPowerupSpawnInterval);
	}

	private void GetNextMonsterSpawnTime() {
		nextMonsterSpawnTime = Time.time + Random.Range(minMonsterSpawnInterval, maxMonsterSpawnInterval);
	}

}
