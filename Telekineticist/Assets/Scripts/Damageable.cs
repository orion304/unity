﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Damageable : MonoBehaviour {

	private static readonly float undamageablePeriod = 0.5f;
	private static readonly float minDamage = 1f;

	public float maxHealth = 500f;
	
	internal float health;
	internal bool isDead = false;

	private Rigidbody rigidBody;
	private NavMeshAgent agent;
	private float lastDamageTime;

	private void Awake() {
		health = maxHealth;
		isDead = false;

		rigidBody = GetComponent<Rigidbody>();
		agent = GetComponent<NavMeshAgent>();
	}

	public void Damage(float damage) {
		if (isDead || damage <= minDamage || Time.time < lastDamageTime + undamageablePeriod) {
			return;
		}

		Debug.Log(damage);

		AlterHealth(-damage);
		lastDamageTime = Time.time;

		if (health <= 0) {
			Kill();
		}
	}

	public virtual void AlterHealth(float amount) {
		health += amount;

		if (health < 0) {
			health = 0;
		} else if (health > maxHealth) {
			health = maxHealth;
		}
	}

	protected virtual void Kill() {
		health = 0;
		isDead = true;

		if (rigidBody != null) {
			rigidBody.freezeRotation = false;
		}

		if (agent != null) {
			agent.enabled = false;
		}
	}
	
}
