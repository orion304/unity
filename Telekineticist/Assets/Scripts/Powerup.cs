﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Powerup : MonoBehaviour {

	private static readonly float healthRestorePercentage = .25f;
	private static readonly float powerRestorePercentage = .25f;
	private static readonly float powerUpgradePercentage = .1f;
	private static readonly float freeTelekinesisDuration = 10f;
	private static readonly float throwForceUpgradePercentage = .05f;
	private static readonly float speedBoostPercentage = 1.5f;
	private static readonly float speedBoostDuration = 10f;

	public enum PowerupType {
		HealthRestore,
		PowerRestore,
		PowerUpgrade,
		FreeTelekinesis,
		ThrowForceUpgrade,
		SpeedBoost
	}

	private static readonly PowerupType[] typeRequiresTelekinesisController = {
		PowerupType.PowerRestore,
		PowerupType.PowerUpgrade,
		PowerupType.FreeTelekinesis,
		PowerupType.ThrowForceUpgrade
	};

	private static readonly PowerupType[] typeRequiresPlayerHealth = {
		PowerupType.HealthRestore
	};

	private static readonly PowerupType[] typeRequiresPlayerMovement = {
		PowerupType.SpeedBoost
	};

	static Powerup() {
		Array.Sort(typeRequiresPlayerHealth);
		Array.Sort(typeRequiresPlayerMovement);
		Array.Sort(typeRequiresTelekinesisController);
	}

	public PowerupType powerupType;

	private void OnTriggerEnter(Collider collider) {
		GameObject triggeringObject = collider.gameObject;

		if (Array.BinarySearch(typeRequiresPlayerHealth, powerupType) >= 0) {
			HandlePowerup(triggeringObject.GetComponent<PlayerHealth>());
			return;
		}

		if (Array.BinarySearch(typeRequiresPlayerMovement, powerupType) >= 0) {
			HandlePowerup(triggeringObject.GetComponent<PlayerMovement>());
			return;
		}

		if (Array.BinarySearch(typeRequiresTelekinesisController, powerupType) >= 0) {
			HandlePowerup(triggeringObject.GetComponent<TelekinesisController>());
			return;
		}
	}

	private void HandlePowerup(PlayerHealth playerHealth) {
		if (playerHealth == null) {
			return;
		}

		switch (powerupType) {
			case PowerupType.HealthRestore:
				playerHealth.AlterHealth(playerHealth.maxHealth * healthRestorePercentage);
				break;
		}

		Kill();
	}

	private void HandlePowerup(PlayerMovement playerMovement) {
		if (playerMovement == null) {
			return;
		}

		switch (powerupType) {
			case PowerupType.SpeedBoost:
				playerMovement.SpeedBoost(speedBoostPercentage, speedBoostDuration);
				break;
		}

		Kill();
	}

	private void HandlePowerup(TelekinesisController telekinesisController) {
		if (telekinesisController == null) {
			return;
		}

		switch (powerupType) {
			case PowerupType.PowerUpgrade:
				float powerChange = telekinesisController.maxPower * powerUpgradePercentage;
				telekinesisController.maxPower += powerChange;
				telekinesisController.power += powerChange;
				break;
			case PowerupType.PowerRestore:
				telekinesisController.power += powerRestorePercentage * telekinesisController.maxPower;
				break;
			case PowerupType.ThrowForceUpgrade:
				telekinesisController.throwForce *= (1 + throwForceUpgradePercentage);
				break;
			case PowerupType.FreeTelekinesis:
				telekinesisController.StartFreeTelekinesis(freeTelekinesisDuration);
				break;
		}

		Kill();
	}

	private void Kill() {
		Destroy(gameObject);
	}


}
