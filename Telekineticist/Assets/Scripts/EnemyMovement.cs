﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyMovement : MonoBehaviour {

	private Transform player;
	private NavMeshAgent nav;

	private void Awake() {
		player = GameObject.FindGameObjectWithTag("Player").transform;
		nav = GetComponent<NavMeshAgent>();
	}

	private void Update() {

		if (nav.enabled) {
			nav.SetDestination(player.position);
		}

	}


}
