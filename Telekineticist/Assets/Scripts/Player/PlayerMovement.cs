﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {

	private static readonly float gravity = Physics.gravity.magnitude;

	public float jumpVelocityControlFactor = 0.03f;
	public float speed = 6.0f;
	public float sensitivity = 2.0f;
	public float maxVelocityChange = 6.0f;
	public float jumpHeight = 1.5f;

	private Rigidbody playerRigidBody;
	private Camera playerCamera;
	private float jumpSpeed;
	private float originalSpeed;

	private bool grounded = false;
	private bool shouldJump = false;
	private float h, v;

	private bool isSpeedBoosted = false;
	private float endSpeedBoostTime;

	private void Awake() {

		Cursor.visible = false;
		Cursor.lockState = CursorLockMode.Locked;

		playerRigidBody = GetComponent<Rigidbody>();
		playerCamera = GetComponentInChildren<Camera>();

		jumpSpeed = Mathf.Sqrt(2 * jumpHeight * gravity);
		originalSpeed = speed;
	}

	private void Update() {
		
		if (isSpeedBoosted) {
			if (Time.time >= endSpeedBoostTime) {
				isSpeedBoosted = false;
				speed = originalSpeed;
			} else {
				//TODO HUD
			}
		}

		if (Input.GetButtonDown("Cancel")) {
			UnityEditor.EditorApplication.isPlaying = false;
		}

		if (Input.GetButtonDown("Jump")) {
			shouldJump = true;
		}

		h = Input.GetAxisRaw("Horizontal");
		v = Input.GetAxisRaw("Vertical");

		float right = Input.GetAxis("Mouse X");
		float up = -Input.GetAxis("Mouse Y");
		Look(right, up);

	}

	private void FixedUpdate() {
		
		Move(h, v);
		
	}

	private void Move(float h, float v) {

		Vector3 targetVelocity = new Vector3(h, 0f, v);
		targetVelocity = targetVelocity.normalized * speed;
		targetVelocity = transform.TransformDirection(targetVelocity);

		Vector3 velocity = playerRigidBody.velocity;
		Vector3 velocityChange = (targetVelocity - velocity);
		velocityChange.x = Mathf.Clamp(velocityChange.x, -maxVelocityChange, maxVelocityChange);
		velocityChange.z = Mathf.Clamp(velocityChange.z, -maxVelocityChange, maxVelocityChange);

		if (shouldJump && grounded) {
			velocityChange.y = jumpSpeed;
		} else {
			velocityChange.y = 0;
		}

		if (!grounded) {
			velocityChange *= jumpVelocityControlFactor;
		}

		playerRigidBody.AddForce(velocityChange, ForceMode.VelocityChange);
		
		shouldJump = false;
		grounded = false;

	}

	private void OnCollisionStay(Collision collision) {
		grounded = true;
	}

	private void Look(float right, float up) {

		if (Input.GetButton("PushPullExpandContract") || Input.GetButton("PullTogetherPushApart")) {
			return;
		}

		playerRigidBody.gameObject.transform.Rotate(0f, right * sensitivity, 0f);
		playerCamera.transform.Rotate(up * sensitivity, 0f, 0f);

	}

	internal void SpeedBoost(float multiplier, float duration) {
		if (isSpeedBoosted) {
			endSpeedBoostTime += duration;
		} else {
			endSpeedBoostTime = Time.time + duration;
		}

		speed = originalSpeed * multiplier;
	}

}
