﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealth : Damageable {

	public Slider healthSlider;

	protected override void Kill() {
		base.Kill();
		SetSlider();
	}

	public override void AlterHealth(float amount) {
		base.AlterHealth(amount);
		SetSlider();
	}

	private void SetSlider() {
		float h = health / maxHealth;

		Debug.Log(string.Format("HP: {0}, Max: {1}, %: {2}", health, maxHealth, h));

		healthSlider.normalizedValue = h;
	}

}
