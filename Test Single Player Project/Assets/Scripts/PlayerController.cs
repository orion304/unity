﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

	public float speed = 2f;
	public float sensitivity = 2f;
	public Camera playerCamera;

	private CharacterController player;
	
	private float moveFB;
	private float moveLR;

	private float rotX;
	private float rotY;

	private void Start() {

		player = GetComponent<CharacterController>();

	}

	private void Update() {

		moveFB = Input.GetAxis("Vertical") * speed;
		moveLR = Input.GetAxis("Horizontal") * speed;
		
		rotX = Input.GetAxis("Mouse X") * sensitivity;
		rotY = Input.GetAxis("Mouse Y") * sensitivity;

		Vector3 movement = new Vector3(moveLR, 0, moveFB);
		movement = transform.rotation * movement;

		transform.Rotate(0, rotX, 0);
		player.Move(movement * Time.deltaTime);

		playerCamera.transform.Rotate(-rotY, 0, 0);

		
	}


}
